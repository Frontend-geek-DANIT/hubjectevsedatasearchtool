let jsonData = null;

document.getElementById('fileInput').addEventListener('change', function(event) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = function(e) {
        try {
            jsonData = JSON.parse(e.target.result);
        } catch (error) {
            alert('Error parsing JSON file. Please upload a valid JSON file.');
            jsonData = null;
        }
    };

    reader.readAsText(file);
});

function searchJSON() {
    const searchInput = document.getElementById('searchInput').value;
    const resultsDiv = document.getElementById('results');
    resultsDiv.innerHTML = '';

    const jsonTable = document.getElementById('jsonTable');
    const tbody = jsonTable.getElementsByTagName('tbody')[0];
    tbody.innerHTML = '';

    if (!jsonData) {
        alert('Please upload a JSON file first.');
        return;
    }

    const operatorEvseData = jsonData.EvseData.OperatorEvseData;
    const foundRecords = [];

    const searchEvseIDs = searchInput.split('\n').map(id => id.trim());

    operatorEvseData.forEach(operator => {
        operator.EvseDataRecord.forEach(record => {
            if (searchEvseIDs.includes(record.EvseID)) {
                foundRecords.push(record);

                // Create a new row in the table for each matching record
                const row = tbody.insertRow();
                const evseIDCell = row.insertCell(0);
                const authModesCell = row.insertCell(1);
                const powerTypeCell = row.insertCell(2);
                const voltageCell = row.insertCell(3);
                const amperageCell = row.insertCell(4);
                const powerCell = row.insertCell(5);
                const accessibilityCell = row.insertCell(6);
                const coordinatesCell = row.insertCell(7);
                const addressCell = row.insertCell(8);

                evseIDCell.textContent = record.EvseID;
                authModesCell.textContent = record.AuthenticationModes.join(', ');
                powerTypeCell.textContent = record.ChargingFacilities[0].PowerType;
                voltageCell.textContent = record.ChargingFacilities[0].Voltage;
                amperageCell.textContent = record.ChargingFacilities[0].Amperage;
                powerCell.textContent = record.ChargingFacilities[0].Power;
                accessibilityCell.textContent = record.Accessibility;
                coordinatesCell.textContent = record.GeoCoordinates.Google.Coordinates;
                addressCell.textContent = `${record.Address.Street}, ${record.Address.City}, ${record.Address.Country}, ${record.Address.PostalCode}`;
            }
        });
    });

    if (foundRecords.length === 0) {
        resultsDiv.innerHTML = 'No matching records found.';
    }
}